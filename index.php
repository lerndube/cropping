<?php 
$id = isset($_GET['id']) ? $_GET['id'] : "";
?>
<!DOCTYPE html>
<html>
<head>
	<script src=" https://code.jquery.com/jquery-2.1.3.min.js"></script>
	
	<title>Image Crop</title>
</head>
<body>
	
	<div class="box-content nopadding">
		<div class="tab-content padding tab-content-inline tab-content-bottom">
			<div class="tab-pane active" id="profile">
				<form action="" method="post" enctype="multipart/form-data" class="form-horizontal form-validate" id="bb">
					<div class="row">
						<div class="col-sm-10">
							<div class="form-actions">
								<div id="cropContainerModal"></div>
							</div>
							<div class="form-actions">
								<input type="submit" name="submit" class='btn btn-primary' value="Done">
							</div>
						</div>
					</div>			
				</form>
			</div>
		</div>
	</div>

	<!--For Image Crop-->
		<link href="croppic/assets/css/main.css" rel="stylesheet">
		<link href="croppic/assets/css/croppic.css" rel="stylesheet">

		<script>
			var tempW = 255;
			var tempH = 400;
		</script>

		<script src="croppic/assets/js/jquery.mousewheel.min.js"></script>
		<script src="croppic/croppic.min.js"></script>
		<script src="croppic/assets/js/main.js"></script>
	<!--End Image Crop-->	


	<script>
	
		var croppicContainerModalOptions = {
			uploadUrl:'croppic/img_save_to_file.php?id=<?php echo $id ?>',
			cropUrl:'croppic/img_crop_to_file.php?id=<?php echo $id ?>',
			modal:true,
			imgEyecandyOpacity:0.4,
			loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> '
		}
		var cropContainerModal = new Croppic('cropContainerModal', croppicContainerModalOptions);
	
	</script>

</body>
</html>