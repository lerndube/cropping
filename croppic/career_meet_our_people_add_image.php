<?php
$p = "career_meet_our_people_add_image";
$act = "career";
session_start();
ob_start();

include "../color.php";
include "../history_function.php";

if(isset($_SESSION['EdsSympAdmPnel']) && ($_SESSION['utype'] == "SuperAdmin" ||  in_array("career", $_SESSION['module'])))
{
   include "../clean_query.php";
   $month = date("m");
   $year = date("Y");
   
	$id = $_GET['id'];
	
	if(isset($_POST['submit']))
		header("Location: ../career_meet_our_people_view.php?msg=add");
   
?>
<!DOCTYPE html>
<html>
<head>
<title>Meet Our People - Infomation Add</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<?php include "../header.php" ?>


</head>

<?php include "../menu.php";?>

		
			<div class="box-content nopadding">
								<h3 style="color: red"><?php echo $errorMsg; ?></h3>
			</div>        
		  
		  <div class="row">
					<div class="col-sm-12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="fa fa-user"></i>
									Meet Our People - Infomation Add
								</h3>
							</div>
							
							<div class="box-content nopadding">
								<div class="tab-content padding tab-content-inline tab-content-bottom">
									<div class="tab-pane active" id="profile">
										<form action="" method="post" enctype="multipart/form-data" class="form-horizontal form-validate" id="bb">
											<div class="row">
												<div class="col-sm-10">
													<div class="form-actions">
														<div id="cropContainerModal"></div>
													</div>
													<div class="form-actions">
														<input type="submit" name="submit" class='btn btn-primary' value="Done">
													</div>
												</div>
											</div>			
										</form>
									</div>
								</div>
							</div>		
						</div>
					</div>
				</div>	
<?php include "../footer.php";?>


<link href="assets/css/main.css" rel="stylesheet">
<link href="assets/css/croppic.css" rel="stylesheet">

<script>
	var tempW = 255;
	var tempH = 400;
</script>
<script src="assets/js/jquery.mousewheel.min.js"></script>
<script src="croppic.min.js"></script>
<script src="assets/js/main.js"></script>
<script>
	
	var croppicContainerModalOptions = {
			cropUrl:'img_crop_to_file.php?id=<?php echo $id ?>',
			modal:true,
			imgEyecandyOpacity:0.4,
			loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> '
	}
	var cropContainerModal = new Croppic('cropContainerModal', croppicContainerModalOptions);
	
</script>

<?php
 
}
else
echo "<meta http-equiv='refresh' content='2;URL=index.php'>\n";
?>
